import "./App.css";
import { AppBar, Toolbar } from "@mui/material";
import { styled } from "@mui/system";
import grabLogo from "./assets/Grab_Logo.png";
import { Outlet } from "react-router-dom";

const MyAppBar = styled(AppBar)({
  backgroundColor: "#b3ffb9",
});

function App() {
  return (
    <>
      <MyAppBar position="fixed">
        <Toolbar>
          <img
            src={grabLogo}
            alt="Grab Logo"
            style={{ height: 40, marginRight: 10 }}
          />
        </Toolbar>
      </MyAppBar>
      <div style={{ paddingTop: 64 }}>
        <Outlet />
      </div>
    </>
  );
}

export default App;
