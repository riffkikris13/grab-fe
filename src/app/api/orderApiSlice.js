import { apiSlice } from "./apiSlice";

export const orderApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getStatistics: builder.query({
      query: () => ({
        url: `/statistics`,
      }),
      providesTags: ["order"],
    }),
    getOrders: builder.query({
      query: ({ stateId, startDate, endDate, page = 0, size = 10 }) => ({
        url: ``,
        params: { stateId, startDate, endDate, page, size },
      }),
      providesTags: ["order"],
    }),
    getOrderStates: builder.query({
      query: () => ({
        url: `/order-states`,
      }),
    }),
    getCustomersByName: builder.query({
      query: ({ name }) => ({
        url: `/customers/${name}`,
      }),
    }),
    addOrder: builder.mutation({
      query: (data) => ({
        url: "",
        method: "POST",
        body: data,
      }),
      invalidatesTags: ["order"],
    }),
    getOrderById: builder.query({
      query: ({ id }) => ({
        url: `/${id}`,
      }),
    }),
    editOrder: builder.mutation({
      query: (data) => ({
        url: "",
        method: "PATCH",
        body: data,
      }),
      invalidatesTags: ["order"],
    }),
  }),
});

export const {
  useGetStatisticsQuery,
  useGetOrdersQuery,
  useGetOrderStatesQuery,
  useGetCustomersByNameQuery,
  useAddOrderMutation,
  useGetOrderByIdQuery,
  useEditOrderMutation,
} = orderApiSlice;
