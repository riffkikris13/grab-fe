import {
  TextField,
  Button,
  Container,
  Box,
  Typography,
  Autocomplete,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
} from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  useAddOrderMutation,
  useGetCustomersByNameQuery,
  useGetOrderStatesQuery,
} from "../app/api/orderApiSlice";

const AddOrder = () => {
  const [formData, setFormData] = useState({
    customer: "",
    state: "",
    address: "",
  });
  const [customerName, setCustomerName] = useState("");

  const navigate = useNavigate();

  const { data: customers } = useGetCustomersByNameQuery(
    { name: customerName },
    {
      skip: !customerName,
    }
  );
  const { data: states } = useGetOrderStatesQuery();
  const [addOrder] = useAddOrderMutation();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await addOrder(formData);
      navigate("/");
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Container maxWidth="sm">
      <Box
        component="form"
        onSubmit={handleSubmit}
        sx={{
          mt: 3,
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
          gap: 1,
        }}
      >
        <Typography gutterBottom sx={{ fontWeight: 500, fontSize: 20 }}>
          Add Order
        </Typography>
        <Autocomplete
          value={formData.customer}
          onChange={(_, newValue) => {
            setFormData((prevFormData) => ({
              ...prevFormData,
              customer: newValue,
            }));
          }}
          inputValue={customerName}
          onInputChange={(_, newInputValue) => {
            setCustomerName(newInputValue);
          }}
          options={customers ?? []}
          isOptionEqualToValue={(opt, val) =>
            opt?.customerName === val?.customerName
          }
          getOptionLabel={(option) => option.customerName ?? ""}
          sx={{ width: "100%" }}
          renderInput={(params) => <TextField {...params} label="Customer" />}
        />
        <FormControl fullWidth>
          <InputLabel id="select-label">State</InputLabel>
          <Select
            labelId="select-label"
            id="select"
            value={formData.state}
            label="State"
            onChange={(e) =>
              setFormData((prevFormData) => ({
                ...prevFormData,
                state: e.target.value,
              }))
            }
            fullWidth
          >
            {states?.map((state) => (
              <MenuItem key={state.id} value={state}>
                {state.stateName}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          label="address"
          value={formData.address}
          onChange={(e) =>
            setFormData((prevFormData) => ({
              ...prevFormData,
              address: e.target.value,
            }))
          }
          fullWidth
          autoComplete="off"
        />
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            width: "100%",
            mt: 5,
          }}
        >
          <Button
            variant="outlined"
            color="error"
            onClick={() => navigate("/")}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            type="submit"
            sx={{ background: "#00B14F" }}
          >
            Submit
          </Button>
        </Box>
      </Box>
    </Container>
  );
};

export default AddOrder;
