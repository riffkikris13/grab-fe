import {
  Container,
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Button,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TablePagination,
} from "@mui/material";
import {
  useGetOrdersQuery,
  useGetStatisticsQuery,
  useGetOrderStatesQuery,
} from "../app/api/orderApiSlice";
import StatisticBox from "./StatisticBox";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { format } from "date-fns";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFnsV3";

function Home() {
  const [state, setState] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const { data: statistics } = useGetStatisticsQuery();
  const { data: orders } = useGetOrdersQuery({
    stateId: state?.id,
    startDate: startDate ? format(startDate, "yyyy-MM-dd'T'HH:mm:ss") : "",
    endDate: endDate ? format(endDate, "yyyy-MM-dd'T'HH:mm:ss") : "",
    page: page,
    size: rowsPerPage,
  });
  const { data: states } = useGetOrderStatesQuery();

  const navigate = useNavigate();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <Container
      sx={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        p: "30px",
        gap: 2,
      }}
    >
      <Box
        gap={3}
        sx={{
          width: 700,
          height: 100,
          display: "flex",
        }}
      >
        <StatisticBox title="Total Orders" data={statistics?.totalOrders} />
        <StatisticBox
          title="Total Ongoing Orders"
          data={statistics?.totalOngoingOrders}
        />
        <StatisticBox
          title="Total Completed Orders"
          data={statistics?.totalCompletedOrders}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: "10px",
        }}
      >
        <Box sx={{ display: "flex", justifyContent: "space-between" }}>
          <Box display="flex" gap={1}>
            <FormControl sx={{ width: "150px" }}>
              <InputLabel id="select-label">State</InputLabel>
              <Select
                labelId="select-label"
                id="select"
                value={state}
                label="State"
                onChange={(e) => setState(e.target.value)}
              >
                {states?.map((state) => (
                  <MenuItem key={state.id} value={state}>
                    {state.stateName}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="Start Date"
                value={startDate}
                onChange={(newValue) => setStartDate(newValue)}
              />
            </LocalizationProvider>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="End Date"
                value={endDate}
                onChange={(newValue) => setEndDate(newValue)}
              />
            </LocalizationProvider>
          </Box>
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#00B14F",
              textTransform: "None",
              width: "fit-content",
            }}
            onClick={() => navigate("/add-order")}
          >
            Add Order
          </Button>
        </Box>
        <TableContainer component={Paper} sx={{ width: 1400 }}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow sx={{ backgroundColor: "#00B14F" }}>
                <TableCell align="center">
                  <Typography sx={{ fontWeight: 400, color: "#fff" }}>
                    Order Id
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography sx={{ fontWeight: 400, color: "#fff" }}>
                    Customer
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography sx={{ fontWeight: 400, color: "#fff" }}>
                    State
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography sx={{ fontWeight: 400, color: "#fff" }}>
                    Address
                  </Typography>
                </TableCell>
                <TableCell align="center">
                  <Typography sx={{ fontWeight: 400, color: "#fff" }}>
                    Order Date
                  </Typography>
                </TableCell>
                <TableCell align="center"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders?.data.map((order) => (
                <TableRow key={order.id}>
                  <TableCell align="center">{order.id}</TableCell>
                  <TableCell align="center">
                    {order.customer.customerName}
                  </TableCell>
                  <TableCell align="center">{order.state.stateName}</TableCell>
                  <TableCell align="center">{order.address}</TableCell>
                  <TableCell align="center">{order.createdDate}</TableCell>
                  <TableCell align="center">
                    <Box
                      sx={{
                        display: "flex",
                        justifyContent: "space-evenly",
                      }}
                    >
                      <Button
                        variant="outlined"
                        sx={{ color: "#b3ffb9", borderColor: "#b3ffb9" }}
                        onClick={() => navigate(`/edit-order/${order.id}`)}
                      >
                        Edit
                      </Button>
                      <Button
                        variant="outlined"
                        sx={{ color: "#b3ffb9", borderColor: "#b3ffb9" }}
                      >
                        Delete
                      </Button>
                    </Box>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        {orders && (
          <TablePagination
            component="div"
            count={orders.totalData}
            page={page}
            onPageChange={handleChangePage}
            rowsPerPage={rowsPerPage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        )}
      </Box>
    </Container>
  );
}

export default Home;
