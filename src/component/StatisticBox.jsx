import { Box, Typography } from "@mui/material";

function StatisticBox({ title, data }) {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      rowGap={1}
      sx={{ flex: 1, backgroundColor: "#00B14F", borderRadius: "10px" }}
    >
      <Typography sx={{ color: "#fff", fontWeight: 400 }}>{title}</Typography>
      <Typography sx={{ color: "#fff", fontWeight: 500, fontSize: "18px" }}>
        {data}
      </Typography>
    </Box>
  );
}

export default StatisticBox;
